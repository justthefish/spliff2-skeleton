<?php
/**
 * Created by VIM
 * @user: thefish
 * @author: anton.gurov@gmail.com
 * User model class
 **/
class Snippets extends Mongo_Model {
    
    public function init() {
        $this->table = DB::me($this)->db->{MONGO_DB_NAME}->snippets;
    }

    public function getList($query=array()) {
        $snippets = array();
        foreach ( $this->table->find($query) as $snippet) {
            $snippets[] = $snippet;
        } 
        return $snippets;
    }
}
