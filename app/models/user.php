<?php
/**
 * Created by VIM
 * @user: thefish
 * @author: anton.gurov@gmail.com
 * User model class
 **/
class User extends Sqlite_Model {
    
    public function init() {
        $this->table('user')->fields(array(
            'id' => array(
                'type' => 'identifier'
            ),
            'login' => array(
                'type' => 'varchar',
                'length' => '255',
                'required' => TRUE,
                'validate' => 'not_empty'
            ),
            'password' => array(
                'type' => 'varchar',
                'length' => '255',
                'required' => TRUE,
                'validate' => 'not_empty'
            ),
            'email' => array(
                'type' => 'varchar',
                'length' => '255',
                'required' => TRUE,
                'validate' => 'not_empty email'
            )
        ));
    }

    public function getByEmail($email) {
        return $this->import(DB::me($this)->select($this)->where('email = \''.$email.'\'')->query());
    }
}
