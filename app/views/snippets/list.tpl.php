<h1>Контейнеры
    <a class="btn btn-primary" href="/snippets/edit/">Добавить новый</a>
</h1>
<table id="applist">
    <thead>
        <tr>
            <th>campaign</th>
            <th>SSPs</th>
            <th>action</th>
        </tr>
    </thead>
    <tbody>
<?php foreach ($list as $item) { ?>
        <tr>
            <td><?php echo $item['campaign'];?></td>
            <td><?php if (isset($item["snippets"]) && is_array($item["snippets"])) {
            
                foreach ($item['snippets'] as $id => $snippet) {
                    echo '<span class="label label-info">'.$id.'</span>&nbsp;'; 
                }} else { 
                    echo "&mdash;";
                } ?>
            </td>
            <td class="span3">
                <span class="pull-right">
                <a href="/snippets/edit?id=<?php echo $item['_id']?>"
                       class="btn btn-inverse">edit</a>
                <a href="/snippets/delete?id=<?php echo $item['_id']?>"
                       class="btn btn-danger"
                       onclick="javascript:return confirm('Уверены?');">
                delete</a>
                <a href="#modal<?php echo $item['campaign'] ;?>"
                    class="btn" data-toggle="modal">script</a>
                </span>

                <div class="modal hide fade" id="modal<?php echo $item['campaign'] ;?>">
                    <div class="modal-header">
                        <div class="close" data-dismiss="modal">x</div>
                        <h5>Скрипт для кампании <?php echo $item['campaign'] ;?></h5>
                        <p>Предупредите клиента, что нужно всталять его в конец страницы</p>
                    </div>
                    <div class="modal-body">
                        <textarea class="codeTA span11" style="height:8em;"><?php echo $item["script"];?></textarea>
                    </div>
                    <div class="modal-footer">
                        <a href="#" data-dismiss="modal" class="btn">Закрыть</a>
                    </div>
                </div>

            </td>
        </tr>
<?php } ?>
    </tbody>
</table>

<script type="text/javascript">
    //JS for dataTable
    $(document).ready(function() {
        //table init
        var table = $('table#applist').dataTable();
        //redraw button
        $('.filter button').click(function(e){
            e.preventDefault();
            table.fnDraw();
        });
        //datepicker
    });
</script>
