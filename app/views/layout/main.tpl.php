<!doctype html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">

    <title>JS Clipper <?php echo $title?></title>
    <meta name="keywords" content="<?php echo $keywords?>">
    <meta name="description" content="<?php echo $description?>">
    <?php foreach ($styles as $path => $data):?>
    <link type="text/css" href="<?php echo BASE_URL.'/assets/static/'.$path?>" rel="stylesheet" media="<?php echo $data['media']?>" />
    <?php endforeach;?>

    <?php foreach ($scripts as $path):?>
    <script type="text/javascript" charset="utf-8" src="<?php echo BASE_URL.'/assets/static/'.$path?>"></script>
    <?php endforeach;?>
</head>
<body>

<div class="navbar navbar-inverse navbar-main">
    <div class="navbar-inner main-navbar">
        <div class="container">
            <a class="brand" href="/">
                JS Clipper
            </a>
            <div class="nav-collapse">
                <?php echo $header;?>
            </div>
        </div>

    </div>
</div>

<div class="container mainbody">
    <div class="row-fluid">
        <div class="span12">
            <?php echo $content;?>
        </div>
    </div>
</div>

<div class="container">
    <footer>
        <div class="container">
            <?php echo $footer; ?>
        </div>
    </footer>
</div>
</body>
</html>
