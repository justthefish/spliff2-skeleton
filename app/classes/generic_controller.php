<?php
/**
 * Created by VIM
 * @user: thefish
 * @author: anton.gurov@gmail.com
 * controller class
 **/
class Generic_Controller {
    
    public $request;
    public $template = 'layout/main';
    public $auth = NULL;
    public $autorender = TRUE;

    public function __construct(Request $request) {
        $this->request = $request;
    }

    public function before() {
        $this->auth = new Auth();
        if($this->autorender == TRUE) {
            $this->template = View::factory($this->template);
            $this->template->title = '';
            $this->template->keywords = '';
            $this->template->description = '';
            $this->template->styles = array();
            $this->template->scripts = array();
            $this->template->header = View::factory('layout/header')->set('user', $this->auth->getUser() );
            $this->template->footer = '';
            $this->template->content = '';
        }
        
    }
    public function after() {

        if($this->autorender == TRUE) {
            $styles = array(
                'style/style.css' => array('media' => 'screen, projection'),
                'bootstrap/css/bootstrap.css' => array('media' => 'screen, projection'),
                'apptable/css/bootstrap-responsive.css' => array('media' => 'screen, projection'),
                'apptable/css/jquery.dataTables.css'=> array('media' => 'screen, projection'),
                'apptable/css/jquery.dataTables_themeroller.css'=> array('media' => 'screen, projection'),
                'apptable/css/appTables.css'=> array('media' => 'screen, projection'),
            );
            $scripts = array(
                'jquery/jquery.min.js',
                'bootstrap/js/bootstrap.js',
                //'bootstrap/js/bootstrap-modal.js',
                'apptable/js/jquery.dataTables.js',
                'apptable/js/apptable-ru.js',
            );

            $this->template->scripts = array_merge($scripts, $this->template->scripts);
            $this->template->styles = array_merge($this->template->styles, $styles);
            
            $this->request->response = $this->template;
        }
    }
}
