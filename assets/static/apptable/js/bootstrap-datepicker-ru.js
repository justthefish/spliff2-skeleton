/*** 
 * Bootstrap datepicker - Russian locale
 * @author anton.gurov@gmail.com
 **/
$.extend($.fn.datepicker.defaults, {
    parse: function (string) {
      var matches;
      //if ((matches = string.match(/^(\d{2,2})\.(\d{2,2})\.(\d{4,4})$/))) {
      if ((matches = string.match(/^(\d{4,4})\-(\d{2,2})\-(\d{2,2})$/))) {
        //month fix
        return new Date(matches[1], matches[2] - 1, matches[3]);
      } else {
        return null;
      }
    },
    format: function (date) {
      var
        month = (date.getMonth() + 1).toString(),
        dom = date.getDate().toString();
      if (month.length === 1) {
        month = "0" + month;
      }
      if (dom.length === 1) {
        dom = "0" + dom;
      }
//      return dom + "." + month + "." + date.getFullYear();
      return date.getFullYear() + "-" + month + "-" + dom;
    },
    startOfWeek: 1,
    monthNames: ["Январь", "Февраль", "Март", "Апрель", 
    "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
    shortDayNames: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
  });  
